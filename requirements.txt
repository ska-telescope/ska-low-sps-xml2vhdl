-i https://pypi.org/simple
colorlog>=5.0.1
pyYAML>=5.4.1
jinja2>=2.10.3
lxml>=4.4.1
numpy>=1.17.3
