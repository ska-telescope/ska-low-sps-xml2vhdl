# -*- coding: utf8 -*-
"""

##################################
``customlogging/customlogging.py``
##################################

``customlogging.py`` is a helpers module provided to define and configure consistent logging. The logging
configuration is handled via ``logging.yml`` YAML file and uses ``colorlog`` for console output.
A rolling system is used for outputting to files.

`BSD`_ Copyright © 2021 UNITED KINGDOM RESEARCH AND INNOVATION

.. _BSD: _static/LICENSE

"""
from pathlib import Path
import sys
import yaml
import logging
import logging.config


def setup_logging(logr_yml, default_level=logging.INFO):
    """Setup logging configuration from a YAML file.

    Args:
        logr_yml(:obj:`str`): defines the relative location of the logging YAML configuration file.
        default_level(:obj:`logging.level`): Optional, sets the default logging level.
            Default value: :obj:`logging.INFO`.
    """
    yml = Path(logr_yml)
    if yml.is_file():
        with yml.open('rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def constant(f):
    """setters and getters for constants used throughout.

    Prevents setting of existing objects. Raises a TypeError.

    Raises:
        TypeError: On setter.

    """

    def fset(value):
        raise TypeError

    def fget():
        return f()

    return property(fget, fset)


class LogLevelsConsts(object):
    """Logger level equivalent strings as constants, used to pass log level to helpers functions.

    Attributes:
        DEBUG (:obj:`str`): String matching DEBUG logger level.
        INFO (:obj:`str`): String matching INFO logger level.
        WARNING (:obj:`str`): String matching WARNING logger level.
        CRITICAL (:obj:`str`): String matching CRITICAL logger level.
        ERROR (:obj:`str`): String matching ERROR logger level.

    """
    @constant
    def DEBUG(self):
        return 'DEBUG'

    @constant
    def INFO(self):
        return 'INFO'

    @constant
    def WARNING(self):
        return 'WARNING'

    @constant
    def CRITICAL(self):
        return 'CRITICAL'

    @constant
    def ERROR(self):
        return 'ERROR'


def config_logger(name, class_name=None, logr_yml='../../support_files/logging.yml', default_level=logging.INFO):
    """Allows a logger to be set up and/or configured in all modules/module classes.

    Args:
        name (:obj:`str`): Name of the logger.
        class_name (:obj:`str`): Optional, name of the class to which the logger is associated.
            Default value: ``None``.
        logr_yml(:obj:`str`): Optional, defines the relative location of the logging YAML configuration file.
             Default value: ``../logging.yml'
        default_level (:obj:`customlogging.LogLevelsConsts`): Optional, default logging level for the logger.
            Default value: :obj:`logging.INFO`.

    Returns:
        :obj:`object`: A configured logger object.

    """
    if class_name is None:
        logr = logging.getLogger('{n}'.format(n=name))
        setup_logging(logr_yml, default_level=default_level)
    else:
        logr = logging.getLogger('{n}.{c}'.format(n=name, c=class_name))

    logr.setLevel(default_level)
    return logr


def logger_sect_break(logr, level=LogLevelsConsts.INFO):
    """Inserts a section break in the logger output.

    Args:
        logr (:obj:`object`): A logger object to pass the section break to.
        level (:obj:`customlogging.LogLevelsConsts`): Optional, logger level of the section break.
            Default value: :obj:`logging.INFO`.

    """
    delimit = '*' * 80
    try:
        if level == LogLevelsConsts.DEBUG:
            logr.debug(delimit)
        elif level == LogLevelsConsts.INFO:
            logr.info(delimit)
        elif level == LogLevelsConsts.WARNING:
            logr.warning(delimit)
        elif level == LogLevelsConsts.CRITICAL:
            logr.critical(delimit)
        elif level == LogLevelsConsts.ERROR:
            logr.error(delimit)
        else:
            logr.info(delimit)
    except AttributeError as e:
        logr.error('{}'
                   .format(__name__))
        logr.error('\t{}'
                   .format(e))


def logger_error_exit(logr):
    """Inserts a exit on error message in the logger output and performs a :obj:`sys.exit`.

    Args:
        logr (:obj:`object`): A logger object to pass the exit on error message to.

    """
    logr.error('')
    logr.error('Exiting...')
    sys.exit(-1)


def logger_mand_missing(obj, field):
    """Inserts a mandatory field missing error message in the logger output and calls :obj:`errorexit`.

    Args:
        obj (:obj:`object`): A logger object to pass the error message to.
        field (:obj:`str`): field to report in the error message.

    """
    obj.logger.error('Missing Mandatory Field in: {obj.settings_file}'
                     .format(obj=obj))
    obj.logger.error('{obj.category}'
                     .format(obj=obj))
    obj.logger.error('  {field}: <VALUE>'
                     .format(field=field))
    logger_error_exit(obj.logger)


def logger_path_missing(obj, name, path):
    """Inserts a path missing error message in the logger output and calls :obj:`errorexit`.

    Args:
        obj (:obj:`object`): A logger object to pass the error message to.
        name (:obj:`str`): Name of processing element which caused the error message.
        path (:obj:`str`): Missing Path which caused the error message.

    """
    obj.logger.error('Processing: {name}'
                     .format(name=name))
    obj.logger.error('Path Missing on File-System: {path}'
                     .format(path=path))
    logger_error_exit(obj.logger)


def disable_logger(log_obj, term):
    """disable the specified logger

    svn logger DEBUG messages contain plain text passwords passed to svn and git.
    To prevent the svn logger from writing DEBUG level messages in the log file disable all ``term``
    related loggers:

    Args:
        log_obj (:obj:`object`): A logger object to pass the error message to.
        term (:obj:`str`): term used to identify logger to disable.

    """
    for name, logr in log_obj.logging.root.manager.loggerDict.iteritems():
        if term in name:
            logr.disabled = True
