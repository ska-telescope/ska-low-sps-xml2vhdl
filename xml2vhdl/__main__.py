# -*- coding: utf8 -*-
from xml2vhdl.arguments import Arguments as Xml2VhdlArguments
import xml2vhdl.xml2vhdl as xml2vhdl


def main():
    xml2vhdl.Xml2VhdlGenerate(Xml2VhdlArguments())
