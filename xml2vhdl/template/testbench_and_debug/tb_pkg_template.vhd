-- This file is generated from a memory map output file from XML2VHDL.
--
-- It contains address offset constants and common functions & procedures for the
-- memory map registers using their id names. Its purpose is to assist in
-- interacting with the memory map registers in testbenches and simulation.
--
-- XML2VHDL is licensed under the BSD 3-Clause license. 
-- See LICENSE file in the project root for details.
-- ---------------------------------------------------------------------------h>

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;
use axi4_lib.axi4lite_bus_cmd_pkg.all;

package {{package_name}} is
--------------------------------------------------------------------------------
-- Constant Declarations:           {# Constants For Addresses #} 
--------------------------------------------------------------------------------

{% for reg in register_list %} constant c_{{reg.upper_id}}_ADDR : std_logic_vector(c_axi4lite_addr_w-1 downto 0) := {{reg.address}}; --{{reg.description}}
{% endfor %}

{% for reg in register_list %}{% if reg.size|int > 1 %}constant c_{{reg.upper_id}}_SIZE : integer := {{reg.size}}; --RAM or External 32b Word Size for {{reg.short_id}}
{% endif %}{% endfor %}


--------------------------------------------------------------------------------
-- Procedure Declarations:          
--------------------------------------------------------------------------------
{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.write_child %}
procedure write_bitfield_{{reg.short_id}}(
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
    {% for child in reg.children %}{% if child.write_perm %};constant {{child.node_id}}					: in {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% if child != reg.children[-1] %}{% endif %}{% endif %}
		{% endfor %});
{% endif %}{% endif %}{% endfor %}

{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.read_child %}
procedure read_bitfield_{{reg.short_id}}(
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
    {% for child in reg.children %}{% if child.read_perm %};variable v_{{child.node_id}}					: out {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% if child != reg.children[-1] %}{% endif %}{% endif %}
		{% endfor %});
{% endif %}{% endif %}{% endfor %}

--Bit Ops Function For Any Reg With Single Bit Field Constituent{% for reg in register_list %} {% if reg.gen_reg_ops %}
procedure bit_ops_{{reg.short_id}}(  --Toggles Single Bit Fields In Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    constant field                      : in    string := "soft_reset";
    constant operation                  : in    string := "assert"
);
{% endif %}{% endfor %}


--Read back any Full Childless Register with Read Permissions{% for reg in register_list %}  {% if reg.read_value or reg.read_partial %}
procedure read_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    variable v_reg_out                : out   std_logic_vector({{reg.range[0]}}-1 downto 0)
);
{% endif %}{% endfor %}

--Write to Childless Register with Read Permissions{% for reg in register_list %}  {% if reg.write_value or reg.write_partial %}
procedure write_{{reg.short_id}}(  --Write Value to The Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    constant reg_value_in                 : in   std_logic_vector({{reg.range[0]}}-1 downto 0)
);
{% endif %}{% endfor %}

--Initialize All Single Writable Registers in an IC in the level below the top IC{% for ic in ic_list %}
procedure initialise_{{ic}}(
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or  reg.write_partial  %};constant {{reg.short_id}}			: in  std_logic_vector({{reg.range[1]}}-1 downto {{reg.range[2]}}-1)	--{{reg.description}}
	{% endif %}{% endif %}{% endfor %}
);
{% endfor %}

end package {{package_name}};

package body {{package_name}} is



--------------------------------------------------------------------------------
-- Procedures:
--------------------------------------------------------------------------------
{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.write_child %}
procedure write_bitfield_{{reg.short_id}}(
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
    {% for child in reg.children %}{% if child.write_perm %};constant {{child.node_id}}					: in {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% if child != reg.children[-1] %}{% endif %}{% endif %}
	{% endfor %}) is
    variable register_var			 : std_logic_vector(c_axi4lite_data_w-1 downto 0) :=(others => '0');
begin
    assert false report "Encoding and Writing {{reg.short_id}} ..." severity note;
    {% for child in reg.children %}{% if child.write_perm %}register_var({% if child.range[0] == 1 %}{{child.range[1] - 1}}{% else %}{{child.range[1] - 1}} downto {{child.range[2] - 1}}{% endif %}) := {{child.node_id}};
    {% endif %}{% endfor %}
	wait until rising_edge(axi4lite_aclk);
	axi4lite_abs_addr_wr32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, c_{{reg.short_id}}_addr,  register_var);

end procedure write_encode_{{reg.short_id}};
{% endif %}{% endif %}{% endfor %}


{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.read_child %}
procedure read_bitfield_{{reg.short_id}}(
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
    {% for child in reg.children %}{% if child.read_perm %};variable v_{{child.node_id}}					: out {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% if child != reg.children[-1] %}{% endif %}{% endif %}
	{% endfor %})is
    variable v_tmp_reg			 : std_logic_vector(c_axi4lite_data_w-1 downto 0) :=(others => '0');
begin
    assert false report "Reading and decoding {{reg.short_id}} ..." severity note;
    axi4lite_read32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, c_{{reg.short_id}}_addr,  v_tmp_reg);
    {% for child in reg.children %}{% if child.read_perm %}v_{{child.node_id}} := v_tmp_reg({% if child.range[0] == 1 %}{{child.range[1] - 1}}{% else %}{{child.range[1] - 1}} downto {{child.range[2] - 1}}{% endif %});
    {% endif %}{% endfor %}
	wait until rising_edge(axi4lite_aclk);

end procedure read_decode_{{reg.short_id}};
{% endif %}{% endif %}{% endfor %}

{% for reg in register_list %} {% if reg.gen_reg_ops %}
procedure bit_ops_{{reg.short_id}}(  --Toggles Single Bit Fields In Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    constant field                      : in    string := "soft_reset";
    constant operation                  : in    string := "assert"
) is

variable v_bitmask                        : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
variable v_retrieved_ctrl_reg             : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
variable v_masked_ctrl_reg                : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');

	begin
	
	if field = "EMPTY"			then v_bitmask := (others => '0');
	{% for child in reg.children %}{% if child.range[0] == 1 %}{% if child.write_perm %} elsif field = "{{child.node_id}}"			then v_bitmask := {{child.mask}}; --bit #{{child.range[1]}}{% endif %}{% endif %}
	{% endfor %}else
        assert false report "Unknown or Unsupported Generator Control Register Field" severity error;
    end if;

    wait until rising_edge(axi4lite_aclk);
	axi4lite_read32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, c_{{reg.short_id}}_addr, v_retrieved_ctrl_reg);
	v_masked_ctrl_reg := bitmask_op(v_retrieved_ctrl_reg, v_bitmask, operation);
	axi4lite_abs_addr_wr32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi,c_{{reg.short_id}}_addr, v_masked_ctrl_reg);
	wait until rising_edge(axi4lite_aclk);

end procedure {{reg.short_id}}_reg_ops;

{% endif %}{% endfor %}

{% for ic in ic_list %}
procedure initialise_{{ic}}(  --Set High Level IC NOTE resets should be performed using the reg_ops function by the user beforehand
    signal axi4lite_aclk                : in    std_logic
    ;signal axi4lite_miso                : in    t_axi4lite_miso
    ;signal axi4lite_mosi                : out   t_axi4lite_mosi
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or  reg.write_partial  %};constant {{reg.short_id}}			: in  std_logic_vector({{reg.range[1]}}-1 downto {{reg.range[2]}}-1)	--{{reg.description}}
	{% endif %}{% endif %}{% endfor %}
) is
begin
    wait until rising_edge(axi4lite_aclk);
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or reg.write_partial %}write_{{reg.short_id}}(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, {{reg.short_id}});
	{% endif %}{% endif %}{% endfor %}wait until rising_edge(axi4lite_aclk);
end procedure {{ic}}_initialise;
{% endfor %}


{% for reg in register_list %}  {% if reg.read_value or reg.read_partial %}
procedure read_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    variable v_reg_out                  : out   std_logic_vector({{reg.range[0]}}-1 downto 0)
) is 
    variable v_tmp_out : std_logic_vector(c_axi4lite_data_w-1 downto 0);
begin
	assert false report "Reading {{reg.short_id}} ..." severity note;
	wait until rising_edge(axi4lite_aclk);
    axi4lite_read32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, c_{{reg.short_id}}_addr,  v_tmp_out);
    wait until rising_edge(axi4lite_aclk);
    v_reg_out := v_tmp_out({{reg.range[1]}}-1 downto {{reg.range[2]}}-1);
    wait until rising_edge(axi4lite_aclk);
    assert false report "Done Reading {{reg.short_id}} Stats" severity note;
end procedure read_{{reg.short_id}};
{% endif %}{% endfor %}


{% for reg in register_list %}  {% if reg.write_value or reg.write_partial %}
procedure write_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal axi4lite_aclk                : in    std_logic;
    signal axi4lite_miso                : in    t_axi4lite_miso;
    signal axi4lite_mosi                : out   t_axi4lite_mosi;
    constant reg_value_in               : in   std_logic_vector({{reg.range[0]}}-1 downto 0)
) is
    variable v_tmp_in : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
begin

	assert false report "Writing to {{reg.short_id}} ..." severity note;
	v_tmp_in({{reg.range[1]}}-1 downto {{reg.range[2]}}-1) := reg_value_in;
	wait until rising_edge(axi4lite_aclk);
	axi4lite_abs_addr_wr32(axi4lite_aclk, axi4lite_miso, axi4lite_mosi, c_{{reg.short_id}}_addr,  v_tmp_in);
    wait until rising_edge(axi4lite_aclk);
    assert false report "Finished Write to {{reg.short_id}}" severity note;
end procedure write_{{reg.short_id}};
{% endif %}{% endfor %}


end package body  {{package_name}};

