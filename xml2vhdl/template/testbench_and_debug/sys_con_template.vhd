
proc combine_count {upper_addr lower_addr} {
global mm

set upper_v	[expr [master_read_32 $mm $upper_addr 1] * 2**32]

return [format %x [expr $upper_v + [expr [ master_read_32 $mm $lower_addr 1]]]

}

proc read_binary_reg {addr} {
	global mm
	set bits [format %032s [hex2bin [string trimleft [master_read_32 $mm $addr 1] 0x]]]
	return $bits
}

proc get_bit {bin_reg loc} {
	set loc2 [expr 32 - $loc]
	return [string index $bin_reg $loc2]
}

proc assert_bit {bin_reg loc} {
	set loc2 [expr 32 - $loc]
	return [string range $bin_reg 0 [expr $loc2 - 1]]1[string range $bin_reg [expr $loc2 + 1] 31]
}

proc deassert_bit {bin_reg loc} {
	set loc2 [expr 32 - $loc]
	return [string range $bin_reg 0 [expr $loc2 - 1]]0[string range $bin_reg [expr $loc2 + 1] 31]

}

proc bin2hex bin {
    ## No sanity checking is done
    array set t {
	0000 0 0001 1 0010 2 0011 3 0100 4
	0101 5 0110 6 0111 7 1000 8 1001 9
	1010 a 1011 b 1100 c 1101 d 1110 e 1111 f
    }
    set diff [expr {4-[string length $bin]%4}]
    if {$diff != 4} {
        set bin [format %0${diff}d$bin 0]
    }
    regsub -all .... $bin {$t(&)} hex
    return [subst $hex]
}


proc hex2bin {hex} {
  set hex2bin_map {
  0  0000
  1  0001
  2  0010
  3  0011
  4  0100
  5  0101
  6  0110
  7  0111
  8  1000
  9  1001
  a  1010
  b  1011
  c  1100
  d  1101
  e  1110
  f  1111
}

  if {![string is xdigit $hex]} {
    error "value \"$hex\" is not a hex integer"
  }
  return [string map -nocase $hex2bin_map $hex]
}


proc mwr {addr value} {
global mm

master_write_32 $mm $addr $value
}

proc mwr2 {addr value1 value2} {
global mm

master_write_32 $mm $addr $value1 $value2
}

proc mrd {addr} {
global mm

master_read_32 $mm $addr 1
}

proc return_led_color {dash status led_id} {
if {[expr $status] == 1} {
    dashboard_set_property $dash $led_id text "1"
    return "green"
} else {
    dashboard_set_property $dash $led_id text "0"
    return "red"
}
}

proc toggle_led {dash addr bit led_id} {
    set curr_reg [read_binary_reg $addr]
    set curr_state [get_bit $curr_reg $bit]
    
    if {[expr $curr_state] == 1} {
        set new_reg [deassert_bit $curr_reg $bit]
        set new_state 0
    } else {
        set new_reg [assert_bit $curr_reg $bit]
        set new_state 1
    }
    mwr $addr 0x[bin2hex $new_reg]
    set color [return_led_color $dash $new_state $led_id]
    dashboard_set_property $dash $led_id color $color
}

proc read_led {dash addr bit led_id} {
    set curr_reg [read_binary_reg $addr]
    set curr_state [get_bit $curr_reg $bit]
    set color [return_led_color $dash $curr_state $led_id]
    dashboard_set_property $dash $led_id color $color
}




proc make_reg_widget {dash name parent addr write} {
set group_name "${name}_group"
set label_name "${name}_label"
set text_name "${name}_text"
set b1_name "${name}_b1"
set b2_name "${name}_b2"
dashboard_add $dash $group_name group $parent
dashboard_set_property $dash $group_name itemsPerRow 1
dashboard_set_property $dash $group_name title ""
dashboard_add $dash $label_name label $group_name
dashboard_set_property $dash $label_name text $name
dashboard_add $dash $text_name text $group_name


if {$write == "True"} {
dashboard_add $dash child group $group_name
dashboard_set_property $dash child itemsPerRow 2
dashboard_set_property $dash child title ""
dashboard_add $dash $b1_name button child
dashboard_set_property $dash $b1_name text "Read"
dashboard_add $dash $b2_name button child
dashboard_set_property $dash $b2_name text "Write"
} else {
dashboard_add $dash $b1_name button $group_name
dashboard_set_property $dash $b1_name text "Read"
}

}

proc make_led_widget {dash name parent addr bitloc type} {
    set group_name "${name}_group"
    set label_name "${name}_label"
    set read_name "${name}_b1"
    dashboard_add $dash $group_name group $parent
    dashboard_set_property $dash $group_name itemsPerRow 1
    dashboard_set_property $dash $group_name title ""
    dashboard_set_property $dash $group_name expandableX true
    
    set led_status [get_bit [read_binary_reg $addr] $bitloc]
    dashboard_add $dash $label_name label $group_name
    dashboard_set_property $dash $label_name text $name
    dashboard_add $dash $name led $group_name
    dashboard_set_property $dash $name color [return_led_color $dash $led_status $name]
    dashboard_add $dash $read_name button $group_name
    
    if {$type == "s"} {
        dashboard_set_property $dash $read_name text "Toggle"
    }   else    {
        dashboard_set_property $dash $read_name text "Refresh"
    }

}

proc read_reg {dash text_id addr} {
    set val [mrd $addr]
    puts "Read 32bit from ${addr}, Value: ${val}"
    dashboard_set_property $dash $text_id text [string range $val 2 9]
}

proc write_reg {dash text_id addr} {
    set wr_val [dashboard_get_property $dash $text_id text]
    set val 0x$wr_val
    puts "Write 32bit to ${addr}, Value: ${val}" 
    mwr $addr $val
}

proc read_partial_reg {dash text_id addr length start} {
    set lower_char [expr ((1 - $start)/4) + 9]
    set upper_char [expr $lower_char + 1 - ($length / 4)]
    set full_reg [mrd $addr]
    set field [string range $full_reg $upper_char $lower_char]
    dashboard_set_property $dash $text_id text [string range $full_reg $upper_char $lower_char ]
    puts "Read Partial from ${addr}, Full: ${full_reg}, Field: ${field}" 
}

proc write_partial_reg {dash text_id addr length start} {
    set val [dashboard_get_property $dash $text_id text]
    if {[expr [string length $val] > [expr $length / 4]]} {
        dashboard_set_property $dash $text_id text "Too Large"
    } else {
        set val2 [format %0*s [expr $length/4] $val]
        set old_reg [mrd $addr]
        set lower_char [expr ((1 - $start)/4) + 9]
        set upper_char [expr $lower_char + 1 - ($length / 4)]
        set new_reg [string replace $old_reg $upper_char $lower_char $val2]
        puts "Write Partial to ${addr}, Before: ${old_reg}, Now: ${new_reg}"
        mwr $addr $new_reg
    }
}


set mm_path [lindex [get_service_paths master] 0]
set mm [claim_service master $mm_path ""]
puts $mm


set dash [add_service dashboard dashboard_example {{package_name}} "Tools/Example"]
dashboard_set_property $dash self visible true


dashboard_add $dash top group self
dashboard_set_property $dash top itemsPerRow 1
dashboard_set_property $dash top title ""
dashboard_set_property $dash top expandableX true

dashboard_add $dash led_indicators group top
dashboard_set_property $dash led_indicators itemsPerRow 8
dashboard_set_property $dash led_indicators title "LED Indicators"

dashboard_add $dash led_switch group top
dashboard_set_property $dash led_switch itemsPerRow 8
dashboard_set_property $dash led_switch title "LED Switch"

dashboard_add $dash registers group top
dashboard_set_property $dash registers itemsPerRow 6
dashboard_set_property $dash registers title "Register Fields"


{% set count = namespace(i=0) %}
{% for item in widget_list %} {% if item.indicator %}
set led_i_{{count.i}} {{item.name}}
make_led_widget $dash $led_i_{{count.i}} led_indicators {{item.addr}} {{item.range[1]}} i
dashboard_set_property $dash "${led_i_{{count.i}}}_b1" onClick {read_led $dash {{item.addr}} {{item.range[1]}} $led_i_{{count.i}}} {% set count.i = count.i + 1 %}
{% endif %}{% endfor %}



{% set count.i = 0 %}{% for item in widget_list %}{% if item.switch %}
set led_s_{{count.i}} {{item.name}}
make_led_widget $dash $led_s_{{count.i}} led_switch {{item.addr}} {{item.range[1]}} s
dashboard_set_property $dash "${led_s_{{count.i}}}_b1" onClick {toggle_led $dash {{item.addr}} {{item.range[1]}}  $led_s_{{count.i}}} {% set count.i = count.i + 1 %}
{% endif %}{% endfor %}



{% set count.i = 0 %}
{% for item in widget_list %}{% if item.r %}
set reg_{{count.i}} {{item.name}}
make_reg_widget $dash $reg_{{count.i}} registers {{item.addr}} {{item.w}}
dashboard_set_property $dash "${reg_{{count.i}}}_b1" onClick {% if item.is_full_reg %}{read_reg $dash "${reg_{{count.i}}}_text" {{item.addr}} }{% else %}{read_partial_reg $dash "${reg_{{count.i}}}_text" {{item.addr}} {{item.range[0]}} {{item.range[2]}} }{% endif %}
{% if item.w %}dashboard_set_property $dash "${reg_{{count.i}}}_b2" onClick {% if item.is_full_reg %}{write_reg $dash "${reg_{{count.i}}}_text" {{item.addr}} }{% else %}{write_partial_reg $dash "${reg_{{count.i}}}_text" {{item.addr}} {{item.range[0]}} {{item.range[2]}} }{% endif %}
{% endif %}{% if item.is_full_reg %}read_reg $dash "${reg_{{count.i}}}_text" {{item.addr}}{% else %}read_partial_reg $dash "${reg_{{count.i}}}_text" {{item.addr}} {{item.range[0]}} {{item.range[2]}}{% endif %}
{% set count.i = count.i + 1 %}{% endif %}{% endfor %}




