-- This file is generated from a memory map output file from XML2VHDL.
--
-- It contains address offset constants and common functions & procedures for the
-- memory map registers using their id names. Its purpose is to assist in
-- interacting with the memory map registers in testbenches and simulation.
--
-- XML2VHDL is licensed under the BSD 3-Clause license. 
-- See LICENSE file in the project root for details.
-- ---------------------------------------------------------------------------h>

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi4_lib;
use axi4_lib.axi4lite_pkg.all;
use axi4_lib.axi4lite_bus_cmd_pkg.all;

package {{package_name}} is
--------------------------------------------------------------------------------
-- Constant Declarations:           {# Constants For Addresses #} 
--------------------------------------------------------------------------------

{% for reg in register_list %} constant c_{{reg.upper_id}}_ADDR : std_logic_vector(c_axi4lite_addr_w-1 downto 0) := {{reg.address}}; --{{reg.description}}
{% endfor %}

{% for reg in register_list %}{% if reg.size|int > 1 %}constant c_{{reg.upper_id}}_SIZE : integer := {{reg.size}}; --RAM or External 32b Word Size for {{reg.short_id}}
{% endif %}{% endfor %}

function check_is_def(sl : in std_logic) return boolean;
function check_is_def(slv : in std_logic_vector) return boolean;
--------------------------------------------------------------------------------
-- Procedure Declarations:          
--------------------------------------------------------------------------------
{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.write_child %}
procedure write_bitfield_{{reg.short_id}}(
    signal bus_i                     : inout    t_axi4lite_bus
    {% for child in reg.children %}{% if child.write_perm %};constant {{child.node_id}}					: in {% if child.range[0] == 1 %}std_logic := 'X'{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0) := (others => 'X'){% endif %}{% endif %}
	{% endfor %});
{% endif %}{% endif %}{% endfor %}

{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.read_child %}
procedure read_bitfield_{{reg.short_id}}(
    signal bus_i                     : inout    t_axi4lite_bus
    {% for child in reg.children %}{% if child.read_perm %};variable v_{{child.node_id}}					: out {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% endif %}
	{% endfor %});
{% endif %}{% endif %}{% endfor %}

--Bit Ops Function For Any Reg With Single Bit Field Constituent{% for reg in register_list %} {% if reg.gen_reg_ops %}
procedure bit_ops_{{reg.short_id}}(  --Toggles Single Bit Fields In Following Reg: {{reg.description}}
    signal bus_i                        : inout    t_axi4lite_bus;
    constant field                      : in    string := "soft_reset";
    constant operation                  : in    string := "assert"
);
{% endif %}{% endfor %}


--Read back any Full Childless Register with Read Permissions{% for reg in register_list %}  {% if reg.read_value or reg.read_partial %}
procedure read_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal bus_i                   : inout    t_axi4lite_bus;
    variable v_data                : out   std_logic_vector({{reg.range[0]}}-1 downto 0)
);
{% endif %}{% endfor %}

--Write to Childless Register with Read Permissions{% for reg in register_list %}  {% if reg.write_value or reg.write_partial %}
procedure write_{{reg.short_id}}(  --Write Value to The Following Reg: {{reg.description}}
    signal bus_i             : inout    t_axi4lite_bus;
    constant data            : in   std_logic_vector({{reg.range[0]}}-1 downto 0)
);
{% endif %}{% endfor %}

--Initialize All Single Writable Registers in an IC in the level below the top IC{% for ic in ic_list %}
procedure initialise_{{ic}}(
    signal bus_i                     : inout    t_axi4lite_bus
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or  reg.write_partial  %};constant {{reg.short_id}}			: in  std_logic_vector({{reg.range[1]}}-1 downto {{reg.range[2]}}-1)	--{{reg.description}}
	{% endif %}{% endif %}{% endfor %}
);
{% endfor %}

end package {{package_name}};

package body {{package_name}} is

--------------------------------------------------------------------------------
-- Functions:
--------------------------------------------------------------------------------
-- Overloaded functions to check for undefined inputs in write_bitfield functions
function check_is_def(sl : in std_logic) return boolean is
begin
    if sl = 'X' then
        return false;
    else
        return true;
    end if;
end function;

function check_is_def(slv : in std_logic_vector) return boolean is
    variable is_defined : boolean := false;  -- Null slv vector will also return '1'
begin
    for i in slv'range loop
        if slv(i) /= 'X' then
            is_defined := true;
        end if;
    end loop;
    return is_defined;
end function;

--------------------------------------------------------------------------------
-- Procedures:
--------------------------------------------------------------------------------
{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.write_child %}
procedure write_bitfield_{{reg.short_id}}(
    signal bus_i                     : inout    t_axi4lite_bus
    {% for child in reg.children %}{% if child.write_perm %};constant {{child.node_id}}					: in {% if child.range[0] == 1 %}std_logic := 'X'{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0) := (others => 'X'){% endif %}{% endif %}
	{% endfor %}) is
    variable v_tmp_reg			 : std_logic_vector(c_axi4lite_data_w-1 downto 0) :=(others => '0');
begin
    assert false report "Writing bitfield reg {{reg.short_id}} ..." severity note;
    axi4lite_bus_rd32(bus_i,c_{{reg.short_id}}_addr, v_tmp_reg);
    {% for child in reg.children %}{% if child.write_perm %}if check_is_def({{child.node_id}}) then v_tmp_reg({% if child.range[0] == 1 %}{{child.range[1] - 1}}{% else %}{{child.range[1] - 1}} downto {{child.range[2] - 1}}{% endif %}) := {{child.node_id}}; end if;
    {% endif %}{% endfor %}
    axi4lite_bus_wr32(bus_i,c_{{reg.short_id}}_addr, v_tmp_reg);

end procedure write_bitfield_{{reg.short_id}};
{% endif %}{% endif %}{% endfor %}


{% for reg in register_list %} {% if reg.numchildren != 0 %}{% if reg.read_child %}
procedure read_bitfield_{{reg.short_id}}(
    signal bus_i                     : inout    t_axi4lite_bus
    {% for child in reg.children %}{% if child.read_perm %};variable v_{{child.node_id}}					: out {% if child.range[0] == 1 %}std_logic{% else %}std_logic_vector({{child.range[0] - 1}}  downto 0){% endif %}{% endif %}
	{% endfor %})is
    variable v_tmp_reg			 : std_logic_vector(c_axi4lite_data_w-1 downto 0) :=(others => '0');
begin
    assert false report "Reading Bitfield reg {{reg.short_id}} ..." severity note;
    axi4lite_bus_rd32(bus_i,c_{{reg.short_id}}_addr, v_tmp_reg);
    {% for child in reg.children %}{% if child.read_perm %}v_{{child.node_id}} := v_tmp_reg({% if child.range[0] == 1 %}{{child.range[1] - 1}}{% else %}{{child.range[1] - 1}} downto {{child.range[2] - 1}}{% endif %});
    {% endif %}{% endfor %}
end procedure read_bitfield_{{reg.short_id}};
{% endif %}{% endif %}{% endfor %}

{% for reg in register_list %} {% if reg.gen_reg_ops %}
procedure bit_ops_{{reg.short_id}}(  --Toggles Single Bit Fields In Following Reg: {{reg.description}}
    signal bus_i                     : inout    t_axi4lite_bus;
    constant field                      : in    string := "soft_reset";
    constant operation                  : in    string := "assert"
) is
    variable v_bitmask                        : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
    variable v_retrieved_ctrl_reg             : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
    variable v_masked_ctrl_reg                : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
begin

	if field = "EMPTY"			then v_bitmask := (others => '0');
	{% for child in reg.children %}{% if child.range[0] == 1 %}{% if child.write_perm %}elsif field = "{{child.node_id}}"			then v_bitmask := {{child.mask}}; --bit #{{child.range[1]}}{% endif %}{% endif %}
	{% endfor %}
	else
        assert false report "Unknown or Unsupported Generator Control Register Field" severity error;
    end if;

    axi4lite_bus_rd32(bus_i,c_{{reg.short_id}}_addr, v_retrieved_ctrl_reg);
	v_masked_ctrl_reg := bitmask_op(v_retrieved_ctrl_reg, v_bitmask, operation);
	axi4lite_bus_wr32(bus_i,c_{{reg.short_id}}_addr, v_masked_ctrl_reg);
end procedure bit_ops_{{reg.short_id}};

{% endif %}{% endfor %}

{% for ic in ic_list %}
procedure initialise_{{ic}}(  --Set High Level IC NOTE resets should be performed using the reg_ops function by the user beforehand
    signal bus_i                     : inout    t_axi4lite_bus
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or  reg.write_partial  %};constant {{reg.short_id}}			: in  std_logic_vector({{reg.range[1]}}-1 downto {{reg.range[2]}}-1)	--{{reg.description}}
	{% endif %}{% endif %}{% endfor %}
) is
begin
	{% for reg in register_list %}{% if reg.parent_ic == ic %}{% if reg.write_value or reg.write_partial %}write_{{reg.short_id}}(bus_i, {{reg.short_id}});
	{% endif %}{% endif %}{% endfor %}
end procedure initialise_{{ic}};
{% endfor %}


{% for reg in register_list %}  {% if reg.read_value or reg.read_partial %}
procedure read_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal bus_i                     : inout    t_axi4lite_bus;
    variable v_data                  : out   std_logic_vector({{reg.range[0]}}-1 downto 0)
) is 
    variable v_tmp_data : std_logic_vector(c_axi4lite_data_w-1 downto 0);
begin
	assert false report "Reading {{reg.short_id}} ..." severity note;
	axi4lite_bus_rd32(bus_i,c_{{reg.short_id}}_addr, v_tmp_data);
    v_data := v_tmp_data({{reg.range[1]}}-1 downto {{reg.range[2]}}-1);
end procedure read_{{reg.short_id}};
{% endif %}{% endfor %}


{% for reg in register_list %}  {% if reg.write_value or reg.write_partial %}
procedure write_{{reg.short_id}}(  --Reads Value in The Following Reg: {{reg.description}}
    signal bus_i                : inout    t_axi4lite_bus;
    constant data               : in   std_logic_vector({{reg.range[0]}}-1 downto 0)
) is
    variable v_tmp_data : std_logic_vector(c_axi4lite_data_w-1 downto 0) := (others => '0');
begin
	assert false report "Writing to {{reg.short_id}} ..." severity note;
	v_tmp_data({{reg.range[1]}}-1 downto {{reg.range[2]}}-1) := data;
	axi4lite_bus_wr32(bus_i,c_{{reg.short_id}}_addr, v_tmp_data);
end procedure write_{{reg.short_id}};
{% endif %}{% endfor %}


end package body  {{package_name}};

