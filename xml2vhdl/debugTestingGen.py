# -*- coding: utf8 -*-
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader
import xml.etree.ElementTree as ET
import os
import sys

from xml2vhdl.customlogging import customlogging as xml2vhdl_logging
from xml2vhdl.version import __version__ as version

if sys.platform.lower() == 'linux':
    logger_yml_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                   '../../../../xml2vhdl-ox/logging.yml'))
    template_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                   '../../../../xml2vhdl-ox/template'))
else:
    logger_yml_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                   '../../../xml2vhdl-ox/logging.yml'))
    template_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                 '../../../xml2vhdl-ox/template'))

logger = xml2vhdl_logging.config_logger(name=__name__, logr_yml=logger_yml_file)
__str__ = "{name} {version}".format(name=__name__, version=version)


# returns [LENGTH UPPER LOWER] bit positions 1 to 32
def mask_to_range(str):
    if str is not None:
        mask = str[2:]

        if mask == "FFFFFFFF":
            return [32, 32, 1]
        elif mask == "FFFF0000":
            return [16, 32, 17]
        elif mask == "0000FFFF":
            return [16, 16, 1]
        else:
            range_int = [0, 0, 0]
            binary_mask = "{0:032b}".format(int(mask, 16))

            for i in range(0, len(binary_mask)):
                if binary_mask[i] == '1':
                    range_int[0] += 1
                    if range_int[1] == 0:
                        range_int[1] = 32 - i

            range_int[2] = range_int[1] - range_int[0] + 1
            return range_int


class RegisterTop:
    def __init__(self, attributes, node_id, address, parent_ic):
        self.description = attributes.get('description')
        mask = attributes.get('mask')
        if not mask == None:
            self.mask = mask[2:]
        self.range = mask_to_range(mask)
        self.node_id = node_id
        self.short_id = node_id
        self.upper_id = ""
        self.address = "X\"" + address + "\""
        self.children = []
        self.parent_ic = parent_ic
        self.read_value = False
        self.write_value = False
        self.read_partial = False
        self.write_partial = False
        self.read_child = False
        self.write_child = False
        self.gen_reg_ops = False
        self.permission = attributes.get('permission')
        self.size = attributes.get('size')

    def change_id(self):
        self.short_id = self.parent_ic + "_" + self.node_id.split('.')[-1]
        self.upper_id = self.short_id.upper()
        self.node_id = self.node_id.split('.', 1)[-1]
        self.node_id = self.node_id.replace(".", "_")

    def add_child(self, child):
        self.children.append(child)

    def set_numchildren(self, numchildren):
        self.numchildren = numchildren

    def check_read_write(self):
        if self.range is not None:
            if self.numchildren == 0 and self.range[0] != 32:
                partial_single = True
            else:
                partial_single = False
            if self.numchildren == 0 and self.permission != "w":
                self.read_value = not partial_single
                self.read_partial = partial_single
                self.gen_reg_ops = False
            if self.numchildren == 0 and self.permission != "r":
                self.write_value = not partial_single
                self.write_partial = partial_single
                self.gen_reg_ops = False
        if self.numchildren != 0:
            for child in self.children:
                if child.read_perm:
                    self.read_child = True
                if child.write_perm:
                    self.write_child = True

    def reg_ops(self):
        for child in self.children:
            if child.range[0] == 1:
                self.gen_reg_ops = True
                break

    def enumerate_name(self, enum):
        self.short_id = self.short_id + str(enum)
        self.upper_id = self.short_id.upper()


class RegisterChild:
    def __init__(self, mask, node_id, description, permission):
        self.mask = "X\"" + mask[2:] + "\""
        self.range = mask_to_range(mask)
        self.node_id = node_id
        self.upper_id = node_id.upper()
        self.description = description
        self.read_perm = False
        self.write_perm = False
        if "r" in permission:
            self.read_perm = True
        if "w" in permission:
            self.write_perm = True


class RegInfo:
    def __init__(self, description, length):
        self.description = description
        self.length = length


class WidgetType:
    # Class Which Extracts Relevant Widget Information From XML Node
    def __init__(self, node, is_full_reg, type, addr=None, parent=None):

        self.desc = node.get("desciption")
        self.range = mask_to_range(node.get("mask"))
        self.indicator = False
        self.switch = False
        self.is_full_reg = is_full_reg

        # XML Table Can Either Be Comprised of IC or a Single Table, With Slightly Differing Layout
        if type == "single":
            # Partial Field Parent Reg ID To Field ID to Get Widget Name
            if parent:
                self.name = parent + "_" + node.get("id")
            else:
                self.name = node.get("id")

            # Partial Field Nodes Need To Be Passed Their Parent Reg Address In Single Tables
            if addr:
                self.addr = addr
            else:
                self.addr = node.get("address")
        else:
            # Reduce Long IDs Which Have Been Compounded With Multiple IC Names
            long_id = node.get("absolute_id")
            if "ic" in long_id:
                node_id = long_id.split("ic")[-1][1::]
            else:
                node_id = long_id.split(".")[-1]
            self.name = node_id.replace(".", "_")
            self.addr = "0x" + node.get("absolute_offset")

        tcl = node.get("tcl")
        self.w = "w" in tcl
        self.r = ("r" in tcl) or self.w
        self.indicator = "i" in tcl
        self.switch = "s" in tcl

        if self.r and ((self.range[0] % 4 != 0) or ((self.range[2] - 1) % 4 != 0)):
            self.logger.critical("Warning: Partial Registers Must Line Up on Nibble Boundaries")
            self.r = False


class DebugTestingFileGenerate:
    def __init__(self, compile_order, relative_output_path, vhdl_output, board_xml_file, gen_tb_packages, tb_package_exclude_list):
        self.logger = xml2vhdl_logging.config_logger(name=__name__, class_name=self.__class__.__name__,
                                                     logr_yml=logger_yml_file)

        # These Give The Names and location of Templates used to generate files

        self.relative_sys_con_template_dir = os.path.join(template_path,
                                                         'testbench_and_debug')

        self.sys_con_template_name = 'sys_con_template.vhd'
        self.relative_tb_pkg_template_dir = os.path.join(template_path,
                                                         'testbench_and_debug')

        self.tb_pkg_template_name = 'tb_pkg_template_v2.vhd'

        if board_xml_file != "":
            board_xml_found = False
            for xml in compile_order:
                if board_xml_file == xml['file']:
                    self.logger.info("Generating System Console GUI TCL File...")
                    board_xml_found = True
                    self.generate_system_console_file(xml, vhdl_output, relative_output_path)
                    break
            if not board_xml_found:
                self.logger.critical("Top Level Memory Map \'{file}\' Can Not be Found. \
                                        \nDoes The XML File Exist?".format(file=board_xml_file))
        else:
            self.logger.info("System Console GUI Generation Is Disabled")
        if gen_tb_packages:
            self.logger.info("Generating Testbench Memory Map Operation VHDL Packages...")
            for xml in compile_order:
                if "pre_generated" not in xml['file']:
                    self.generate_testbench_package_files(xml, vhdl_output, relative_output_path, tb_package_exclude_list)

        else:
            self.logger.info("Testbench Memory Map Operation VHDL Package Generation Is Disabled")

    def generate_system_console_file(self, xml, vhdl_output, relative_output_path):
        xml_file_name = xml['file']
        xml_file_path = os.path.dirname(os.path.abspath(xml_file_name))

        if relative_output_path != "":
            xml_output_dir = os.path.join(xml_file_path, relative_output_path)
        else:
            xml_output_dir = vhdl_output

        # Get Generated Memory Map Output For Chosen File
        xml_output_file_name = os.path.basename(xml_file_name).replace(".xml", "_output.xml")
        xml_output_abs_path = os.path.join(xml_output_dir, xml_output_file_name)

        # Construct Output File Paths
        sys_con_relative_path = 'sys_con/'
        sys_con_output_dir = os.path.join(xml_output_dir, sys_con_relative_path)
        sys_con_file_name = os.path.basename(xml_file_name).replace(".xml", "_syscon.tcl")
        sys_con_file = os.path.join(sys_con_output_dir, sys_con_file_name)

        # Find Template for TCL Generation'
        script_dir = os.path.dirname(__file__)
        sys_con_template_dir = os.path.join(script_dir, self.relative_sys_con_template_dir)
        sys_con_template = os.path.join(sys_con_template_dir, self.sys_con_template_name)

        self.logger.info("Generating Sys Con GUI TCL File:  {file}".format(file=sys_con_file))
        self.logger.info("    Using Top Level Resolved XML File :  {file}".format(file=xml_output_abs_path))
        self.logger.info("    And Template : {template}".format(template=sys_con_template))

        # Likely A IC But Need To Check, The Two Have A Different number Of XML Layers
        xml_file = ET.parse(xml_output_abs_path).getroot()
        if xml_file.get('hw_type') == "ic" or xml_file.get('hw_type') == "transparent_ic":
            memory_map_type = "ic"
        else:
            memory_map_type = "single"

        widget_list = []
        if memory_map_type == "single":
            for node in xml_file:
                # Full Register in Single Table
                if list(node) == []:
                    tcl = node.get("tcl")
                    if tcl:
                        temp_reg = WidgetType(node, True, memory_map_type)
                        widget_list.append(temp_reg)
                # Segmented Register in Single Table
                else:
                    parent_id = node.get("id")
                    address = node.get("address")
                    for child_node in node:
                        tcl = child_node.get("tcl")
                        if tcl:
                            temp_reg = WidgetType(child_node, False, memory_map_type, address, parent_id)
                            widget_list.append(temp_reg)
        else:
            for reg in xml_file:
                for node in reg:
                    # Full Register In IC
                    if list(node) == []:
                        tcl = node.get("tcl")
                        if tcl:
                            temp_reg = WidgetType(node, True, memory_map_type)
                            widget_list.append(temp_reg)
                    # Segmented Register In IC
                    else:
                        parent_id = node.get("id")
                        for child_node in node:
                            tcl = child_node.get("tcl")
                            if tcl:
                                temp_reg = WidgetType(child_node, False, memory_map_type)
                                widget_list.append(temp_reg)

        if widget_list:
            if not os.path.exists(sys_con_output_dir):
                os.makedirs(sys_con_output_dir)
            widget_list = sorted(widget_list, key=lambda WidgetType: WidgetType.addr)
            env = Environment(loader=FileSystemLoader(sys_con_template_dir))
            top = env.get_template(self.sys_con_template_name)
            title = xml_output_abs_path.split("/")[-1]
            title = title.split(".")[0]
            output_from_parsed_template = top.render(package_name=title, widget_list=widget_list)
            with open(sys_con_file, "w") as fh:
                fh.write(output_from_parsed_template)
        else:
            self.logger.warning("No TCL Fields in: \n{file} \n TCL File Not Generated".format(file=xml_output_abs_path))

    def generate_testbench_package_files(self, xml, vhdl_output, relative_output_path, exclude_list):

        xml_file_name = xml['file']
        xml_file_path = os.path.dirname(os.path.abspath(xml_file_name))

        if relative_output_path != "":
            xml_output_dir = os.path.abspath(os.path.join(xml_file_path, relative_output_path))
        else:
            xml_output_dir = os.path.abspath(vhdl_output)

        self.logger.info('XML Output Path: {p}'.format(p=xml_output_dir))
        # Get Generated Memory Map Output For Chosen File
        xml_output_file_name = os.path.basename(xml_file_name).replace(".xml", "_output.xml")
        xml_output_abs_path = os.path.join(xml_output_dir, xml_output_file_name)

        # Check if the file exists:
        if not os.path.isfile(xml_output_abs_path):
            self.logger.info("XML file does not exist: {file}".format(file=xml_output_abs_path))
            self.logger.info("Skipping testbench file generation")
            return

        # Construct Output File Paths
        tb_pkg_relative_path = 'tb_packages'
        tb_pkg_output_dir = os.path.join(xml_output_dir, tb_pkg_relative_path)
        tb_pkg_file_name = os.path.basename(xml_file_name).replace(".xml", "_tb_pkg.vhd")
        tb_pkg_file = os.path.join(tb_pkg_output_dir, tb_pkg_file_name)

        self.logger.info("Generating VHDL Testbench Pkg File: {file}".format(file=tb_pkg_file))
        self.logger.info("    Using Resolved XML Memory Map File: {file}".format(file=xml_output_abs_path))

        # Find Template for TB Pkg Generation'
        script_dir = os.path.dirname(__file__)
        tb_pkg_template_dir = os.path.join(script_dir, self.relative_sys_con_template_dir)

        root = ET.parse(xml_output_abs_path).getroot()
        register_list = []
        ic_list = []
        number_of_registers = 0

        if root.get('hw_type') == "ic" or root.get('hw_type') == "transparent_ic":
            memory_map_type = "ic"
        else:
            memory_map_type = "single"

        if memory_map_type == "ic":
            id_list = list()
            for parent in root:
                parent_ic = parent.get('absolute_id').split('.')[-1]
                ic_list.append(parent_ic)
                for middle in parent:
                    node_id = middle.get('absolute_id').lower()
                    if not any(ele in node_id for ele in exclude_list):
                        temp_numchildren = 0
                        temp_reg = RegisterTop(middle, node_id,
                                               middle.get('absolute_offset'), parent_ic)
                        if list(middle) == []:
                            temp_reg.set_numchildren(0)
                        else:
                            for child in middle:
                                temp_numchildren += 1
                                temp_reg.add_child(
                                    RegisterChild(child.get('mask'), child.get('id'), child.get('description'), child.get('permission')))
                                temp_reg.set_numchildren(temp_numchildren)
                            temp_reg.reg_ops()
                            temp_reg.children = sorted(temp_reg.children, key=lambda RegisterChild: RegisterChild.range[2])
                        temp_reg.change_id()
                        temp_reg.check_read_write()
                        register_list.append(temp_reg)
            seen_id = list()
            repeated_id = list()
            repeated = False
            for reg in register_list:
                if reg.short_id in seen_id:
                    repeated_id.append(reg.short_id)
                    repeated = True
                else:
                    seen_id.append(reg.short_id)
            if repeated:
                self.logger.info("Some Registers In This File Have Duplicated generated IDs...")
                self.logger.info("Enumerating Repeated IDs...")
                self.logger.info("Adding Parent IC Name To Repeated Register IDs to Try and Prevent Repeated ID...")

                register_list = sorted(register_list, key=lambda RegisterTop: RegisterTop.address)
                for identifier in repeated_id:
                    enum = 0
                    for reg in register_list:
                        if identifier == reg.short_id:
                            old_id = reg.short_id
                            reg.enumerate_name(enum)
                            self.logger.debug("Duplicate ID in register Set, Renamed:  {old_id} To {id})"
                                              .format(old_id=old_id, id=reg.short_id))
                            enum = enum + 1
        else:
            for middle in root:
                node_id = middle.get('id').lower()
                if not any(ele in node_id for ele in exclude_list):
                    temp_numchildren = 0
                    temp_reg = RegisterTop(middle, node_id,
                                           middle.get('address')[2:], root.get('id'))
                    for child in middle:
                        temp_numchildren += 1
                        temp_reg.add_child(
                            RegisterChild(child.get('mask'), child.get('id'), child.get('description'), child.get('permission')))
                    temp_reg.set_numchildren(temp_numchildren)
                    temp_reg.reg_ops()
                    temp_reg.children = sorted(temp_reg.children, key=lambda RegisterChild: RegisterChild.range[2])
                    temp_reg.change_id()
                    temp_reg.check_read_write()
                    register_list.append(temp_reg)
        if not os.path.exists(tb_pkg_output_dir):
            os.makedirs(tb_pkg_output_dir)

        register_list = sorted(register_list, key=lambda RegisterTop: RegisterTop.address)

        env = Environment(loader=FileSystemLoader(tb_pkg_template_dir))
        top = env.get_template(self.tb_pkg_template_name)
        title = tb_pkg_file_name.split(".")[0]
        output_from_parsed_template = top.render(package_name=title, number_of_registers=number_of_registers,
                                                 register_list=register_list, ic_list=ic_list)
        with open(tb_pkg_file, "w") as fh:
            fh.write(output_from_parsed_template)
