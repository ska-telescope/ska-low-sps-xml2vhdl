# -*- coding: utf8 -*-
__all__ = ['allowed_attribute_value', 'default_attribute_value', 'attribute_description',
           'print_help', 'print_log', 'print_latest_version',
           'reverse_key_order', 'dec_check_bit', 'dec_get_last_bit', 'dec_route_add', 'Slave',
           'hex_format', 'normalize_path', 'normalize_template', 'normalize_output_folder', 'file_list_generate',
           'read_template_file', 'write_vhdl_file', 'get_max_len', 'add_space', 'str_format', 'indent',
           'replace_sig', 'replace_const',
           'vhdl_reserved_words',
           'xml_add', 'xml_fill_node', 'xml_build_output',
           'get_parameters_dict', 'XmlMemoryMap',
           ]

from .help import allowed_attribute_value, default_attribute_value, attribute_description
from .help import print_help, print_log, print_latest_version

from .slave import reverse_key_order, dec_check_bit, dec_get_last_bit, dec_route_add
from .slave import Slave

from .string_io import hex_format, normalize_path, normalize_template, normalize_output_folder, file_list_generate
from .string_io import read_template_file, write_vhdl_file, get_max_len, add_space, str_format, indent
from .string_io import replace_sig, replace_const

from .reserved_word import vhdl_reserved_words

from .xml_gen import xml_add, xml_fill_node, xml_build_output

from .xml_utils import get_parameters_dict
from .xml_utils import XmlMemoryMap